# wanb33f
A fork of stormshadow07's BeEF over WAN rewritten in python3 with a few optimizations, automating some actions designed through a minimal approach.
This tool is used to automate Browser Exploitation Framework (beef-xss) over wide area network using ngrok tunneling to avoid port forwarding.
---
### Prereqisites:
* python3
* beef-xss

### v0.0.3 features:
* Fully automated
* Improved minimalist approach
* Included ngrok binary
* Added configuration file to avoid interference with global settings

### Bugs? Wait! Read this first.
Before submitting any bug request or suggestion, please check if a similar request has already been made.
